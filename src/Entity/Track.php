<?php

namespace App\Entity;

use App\Model\TrackStatistics;
use App\Repository\TrackRepository;
use DateTimeInterface;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

define("PRESET_ORDER", 0);
define("FREE_ORDER", 1);

#[ORM\Entity(repositoryClass: TrackRepository::class)]
class Track
{
    /** @noinspection PhpPropertyOnlyWrittenInspection */
    #[ORM\Column]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    private ?int $id;

    #[ORM\Column(type: Types::STRING, length: 255)]
    private ?string $name;

    /**
     * 0: preset order
     * 1: free order
     */
    #[ORM\Column(type: Types::INTEGER)]
    private ?int $format;

    #[ORM\Column(type: Types::INTEGER)]
    private ?int $totalTime;

    #[ORM\Column(type: Types::JSON)]
    private ?array $punches = [];

    #[ORM\Column(type: Types::TEXT)]
    private ?string $trace;

    #[ORM\ManyToOne(targetEntity: Course::class, inversedBy: "orienteer")]
    #[ORM\JoinColumn(nullable: false)]
    private ?Course $course;

    #[ORM\Column(type: Types::BOOLEAN)]
    private bool $imported = false;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, options: ['default' => 'CURRENT_TIMESTAMP'])]
    private ?DateTimeInterface $date;

    #[ORM\Column(type: Types::BOOLEAN, options: ["default" => false])]
    private bool $cheating = false;

    #[ORM\Column(type: Types::INTEGER, options: ['default' => -1])]
    private int $score = -1;

    #[ORM\Column(type: Types::INTEGER, options: ['default' => -1])]
    private int $runCount = 1;

    public static function WaypointsToString(array $finalWaypoints): string
    {
        $trace = "";
        foreach ($finalWaypoints as $waypoint) {
            $trace .= $waypoint[0] . "," . $waypoint[1] . "," . $waypoint[2] . "," . $waypoint[3] . ";";
        }
        return $trace;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getFormat(): ?int
    {
        return $this->format;
    }

    public function setFormat(int $format): self
    {
        $this->format = $format;

        return $this;
    }

    public function getTotalTime(): ?int
    {
        return $this->totalTime;
    }

    public function setTotalTime(int $totalTime): self
    {
        $this->totalTime = $totalTime;

        return $this;
    }

    public function getTotalTimeInHMS(): string
    {
        $sec = (int)($this->totalTime / 1000);
        $hrs = 0;
        $min = 0;
        if ($sec >= 3600) {
            $hrs = floor($sec / 3600);
            $sec = $sec % 3600;
        }
        if ($sec >= 60) {
            $min = floor($sec / 60);
            $sec = $sec % 60;
        }
        if (strlen((string)$hrs) === 1) {
            $hrs = "0" . $hrs;
        }
        if (strlen((string)$min) === 1) {
            $min = "0" . $min;
        }
        if (strlen((string)$sec) === 1) {
            $sec = "0" . $sec;
        }
        return $hrs . ":" . $min . ":" . $sec;
    }

    public function getPunches(): ?array
    {
        return $this->punches;
    }

    public function setPunches(array $punches): self
    {
        $this->punches = $punches;
        return $this;
    }

    public function getTrace(): ?string
    {
        return $this->trace;
    }

    public function setTrace(string $trace): self
    {
        $this->trace = $trace;
        return $this;
    }

    public function getCourse(): ?Course
    {
        return $this->course;
    }

    public function setCourse(?Course $course): self
    {
        $this->course = $course;
        return $this;
    }

    public function getImported(): ?bool
    {
        return $this->imported;
    }

    public function setImported(bool $imported): self
    {
        $this->imported = $imported;
        return $this;
    }

    public function getDate(): ?DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(DateTimeInterface $date): self
    {
        $this->date = $date;
        return $this;
    }

    public function isCheating(): ?bool
    {
        return $this->cheating;
    }

    public function setCheating(bool $cheating): self
    {
        $this->cheating = $cheating;
        return $this;
    }

    public function getScore(): ?int
    {
        return $this->score;
    }

    public function setScore(int $score): self
    {
        $this->score = $score;
        return $this;
    }

    public function getRunCount(): ?int
    {
        return $this->runCount;
    }

    public function setRunCount(?int $runCount): self
    {
        if ($runCount == null) {
            $this->runCount = 1;
        } else {
            $this->runCount = $runCount;
        }

        return $this;
    }


    public static function detectCheating(array $punchTimes, string $trace, string $xmlContentsAsString): bool
    {
        uasort($punchTimes, function ($a, $b): int {
            return $a["punchTime"] <=> $b["punchTime"];
        });
        $checkpoints = Course::extractCheckpointsFromXML(simplexml_load_string($xmlContentsAsString));
        $waypoints = Track::extractWaypointsFromRouteString($trace);
        $currentPunchTimeIndex = 0;
        for ($i = 0; $i < count($waypoints) && $currentPunchTimeIndex < count($punchTimes); $i++) {
            if ($i == 0 || $waypoints[$i][2] == $punchTimes[$currentPunchTimeIndex]["punchTime"]) {
                $latitude = $waypoints[$i][0];
                $longitude = $waypoints[$i][1];
                ++$currentPunchTimeIndex;
            } else if ($waypoints[$i][2] > $punchTimes[$currentPunchTimeIndex]["punchTime"]) {
                $lat1 = $waypoints[$i - 1][0];
                $lon1 = $waypoints[$i - 1][1];
                $t1 = $waypoints[$i - 1][2];
                $lat2 = $waypoints[$i][0];
                $lon2 = $waypoints[$i][1];
                $t2 = $waypoints[$i][2];
                $tx = $punchTimes[$currentPunchTimeIndex]["punchTime"];
                if ($t2 != $t1) {
                    $ratio = ($tx - $t1) / ($t2 - $t1);
                } else {
                    $ratio = 0.5; // use the center between the 2 locations
                }
                $latitude = $ratio * ($lat2 - $lat1) + $lat1;
                $longitude = $ratio * ($lon2 - $lon1) + $lon1;
                ++$currentPunchTimeIndex;
            } else {
                continue;
            }
            if (isset($punchTimes[$currentPunchTimeIndex - 1]["forced"]) && $punchTimes[$currentPunchTimeIndex - 1]["forced"]) {
                $checkpoint = $checkpoints[$punchTimes[$currentPunchTimeIndex - 1]["controlPoint"]];
                $distance = TrackStatistics::distanceInMeters($latitude, $longitude, $checkpoint->location[0], $checkpoint->location[1]);
                if ($distance > 50) {
                    return true;
                }
            }
        }
        return false;
    }

    public static function computeScore(array $punches, string $xmlContentsAsString): int
    {
        $xmlContents = simplexml_load_string($xmlContentsAsString);
        $score = 0;
        $controlPointIndex = 0;
        foreach ($xmlContents->RaceCourseData->Course->CourseControl as $y) {
            // Do not use the start and end checkpoints
            if ((string)$y->attributes()["type"] === "Control") {
                if ((int)$punches[$controlPointIndex]["punchTime"] > 0) {
                    if (isset($y->Score)) {
                        $score += (int)$y->Score;
                    } else {
                        $score += 1;
                    }
                }
            }
            $controlPointIndex++;
        }
        return $score;
    }

    public static function extractWaypointsFromRouteString(?string $gpxContents): array
    {
        $waypoints = [];
        if ($gpxContents == null) {
            return $waypoints;
        }
        $geodesicPoints = explode(';', $gpxContents);
        foreach ($geodesicPoints as $point) {
            $coordinates = explode(',', $point);
            if (count($coordinates) == 4) {
                $latitude = $coordinates[0];
                $longitude = $coordinates[1];
                $timeInMs = intval($coordinates[2]);
                $altitudeInMeters = $coordinates[3];
                $waypoints[] = [$latitude, $longitude, $timeInMs, $altitudeInMeters];
            }
        }

        return $waypoints;
    }

    public static function computeNumberOfPunchedCheckpoints(array $punches): int
    {
        $score = 0;
        for ($i = 1; $i < count($punches) - 1; $i++) {
            if ((int)$punches[$i]["punchTime"] > 0) {
                $score += 1;
            }
        }
        return $score;
    }
}