<?php

namespace App\Entity;

use App\Repository\LiveTrackingOrienteerRepository;
use DateTimeInterface;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: LiveTrackingOrienteerRepository::class)]
class LiveTrackingOrienteer
{
    /** @noinspection PhpPropertyOnlyWrittenInspection */
    #[ORM\Column]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    private ?int $id;

    #[ORM\Column(type: Types::STRING, length: 255, nullable: true)]
    private ?string $latitudes;

    #[ORM\Column(type: Types::STRING, length: 255, nullable: true)]
    private ?string $longitudes;

    #[ORM\Column(type: Types::INTEGER)]
    private ?int $score;

    #[ORM\Column(type: Types::STRING, length: 255)]
    private ?string $nickname;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?DateTimeInterface $startDate;

    #[ORM\ManyToOne(targetEntity: LiveTracking::class, cascade: ['persist'])]
    private ?LiveTracking $live;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLatitudes(): ?string
    {
        return $this->latitudes;
    }

    public function setLatitudes(string $latitudes): self
    {
        $this->latitudes = $latitudes;

        return $this;
    }

    public function getLongitudes(): ?string
    {
        return $this->longitudes;
    }

    public function setLongitudes(string $longitudes): self
    {
        $this->longitudes = $longitudes;

        return $this;
    }

    public function getScore(): ?int
    {
        return $this->score;
    }

    public function setScore(int $score): self
    {
        $this->score = $score;

        return $this;
    }

    public function getNickname(): ?string
    {
        return $this->nickname;
    }

    public function setNickname(string $nickname): self
    {
        $this->nickname = $nickname;

        return $this;
    }

    public function getStartDate(): ?DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getLive(): ?LiveTracking
    {
        return $this->live;
    }

    public function setLive(?LiveTracking $live): self
    {
        $this->live = $live;

        return $this;
    }
}
