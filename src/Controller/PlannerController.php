<?php

namespace App\Controller;

use App\Entity\Course;
use App\Entity\EventCourse;
use App\Entity\MissingControlPoint;
use App\Entity\ParticipantMakeEventCourse;
use App\Entity\Track;
use App\Entity\User;
use App\Model\CreateCourse;
use App\Model\CreateCourseException;
use App\Model\CreateUser;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Attribute\Route;
use function in_array;

class PlannerController extends AbstractController
{
    #[Route('/api/planner/delete-planner', methods: ['DELETE'])]
    public function delete_planner(EntityManagerInterface $entityManager): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        if ($user == null) {
            return new JsonResponse(
                [
                    "message" => "Unauthorized request",
                ],
                Response::HTTP_UNAUTHORIZED
            );
        }
        $user->remove($entityManager);
        return new JsonResponse();
    }

    #[Route('/api/planner/missing-checkpoints', methods: ['GET'])]
    public function missing_checkpoints(): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        if ($user == null) {
            return new JsonResponse(
                [
                    "message" => "Unauthorized request",
                ],
                Response::HTTP_UNAUTHORIZED
            );
        }
        $missingCheckPoints = [];
        foreach ($user->getCourses() as $course) {
            if (count($course->getMissingControlPoints()) > 0) {
                $missingCheckPoints[] = $course->getName();
            }
        }
        return new JsonResponse(
            [
                "missingCheckpoints" => $missingCheckPoints,
            ],
            Response::HTTP_OK
        );
    }

    #[Route('/api/planner/my-courses/{user_id}', requirements: ['user_id' => '\-?\d+'], defaults: ["user_id" => -1], methods: ['GET'])]
    public function get_all_my_courses(int $user_id, EntityManagerInterface $entityManager): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        if ($user_id >= 0) {
            if ($user->isAdmin()) {
                $user = $entityManager->getRepository(User::class)->find($user_id);
            } else {
                return new JsonResponse(
                    "Unauthorized request",
                    Response::HTTP_UNAUTHORIZED
                );
            }
        }
        if ($user == null) {
            return new JsonResponse(
                "Unauthorized request",
                Response::HTTP_UNAUTHORIZED
            );
        }
        $response = [];

        /** @var Course $course */
        $courses = $entityManager->getRepository(Course::class)->findCoursesWithOrienteerCount($user);

        foreach ($courses as $course) {
            $response[] = [
                "id" => $course->getId(),
                "name" => $course->getName(),
                "private" => $course->isPrivate(),
                "hidden" => $course->isHidden(),
                "closed" => $course->isClosed(),
                "touristic" => $course->getTouristic(),
                "missing_checkpoint_count" => count($course->getMissingControlPoints()),
                "discipline" => $course->getDiscipline(),
                "track_count" => $course->getOrienteerCount(),
            ];
        }
        return new JsonResponse(
            $response,
            Response::HTTP_OK
        );
    }

    #[Route('/api/planner/communities/{user_id}', requirements: ['user_id' => '\-?\d+'], defaults: ["user_id" => -1], methods: ['GET'])]
    public function get_all_communities(int $user_id, EntityManagerInterface $entityManager): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        if ($user_id >= 0) {
            if ($user->isAdmin()) {
                $user = $entityManager->getRepository(User::class)->find($user_id);
            } else {
                return new JsonResponse(
                    "Unauthorized request",
                    Response::HTTP_UNAUTHORIZED
                );
            }
        }
        if ($user == null) {
            return new JsonResponse(
                "Unauthorized request",
                Response::HTTP_UNAUTHORIZED
            );
        }
        $communities = [];
        foreach ($user->getCourses() as $course) {
            if ($course->isOpen() && !$course->isPrivate()) {
                $club = $course->getClub();
                if (!empty($club) && !in_array($club, $communities)) {
                    $communities[] = $club;
                }
            }
        }
        $response = [];
        foreach ($communities as $community) {
            $response[] = [
                "name" => $community
            ];
        }
        return new JsonResponse(
            $response,
            Response::HTTP_OK
        );
    }

    #[Route('/api/planner/my-course/tracks/{course_id}', requirements: ['course_id' => '\d+'], methods: ['GET'])]
    public function get_all_tracks(int $course_id, EntityManagerInterface $entityManager): Response
    {
        $tracks = $entityManager->getRepository(Track::class)->findByCourse($course_id);
        $response = [];
        foreach ($tracks as $track) {
            $response[] = [
                "id" => $track->getId(),
                "name" => $track->getName() . " (" . $track->getTotalTimeInHMS() . ")",
            ];
        }
        return new JsonResponse(
            $response,
            Response::HTTP_OK
        );
    }

    #[Route('/api/planner/my-course/update-hidden-course/{course_id}', requirements: ['course_id' => '\d+'], methods: ['PATCH'])]
    public function update_hidden_course(int $course_id, Request $request, EntityManagerInterface $entityManager): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        $course = $entityManager->getRepository(Course::class)->find($course_id);
        if ($course == null) {
            return new JsonResponse(
                [
                    "message" => "Unauthorized request",
                ],
                Response::HTTP_UNAUTHORIZED
            );
        }
        $creator = $course->getCreator();
        if (!$user->isAdmin() && $creator->getId() !== $user->getId()) {
            return new JsonResponse(
                [
                    "message" => "Unauthorized request",
                ],
                Response::HTTP_UNAUTHORIZED,
            );
        }
        $requestData = json_decode($request->getContent());
        $course->setHidden($requestData->isHidden);
        try {
            $entityManager->persist($course);
            $entityManager->flush();
        } catch (Exception $e) {
            return new JsonResponse(
                $e->getMessage(),
                Response::HTTP_BAD_REQUEST
            );
        }
        return new JsonResponse();
    }

    #[Route('/api/planner/my-course/delete-tracks/{course_id}', requirements: ['course_id' => '\d+'], methods: ['DELETE'])]
    public function deleteTracks(int $course_id, Request $request, EntityManagerInterface $entityManager): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        $tracks = $entityManager->getRepository(Track::class)->findByCourse($course_id);
        $errorMessage = [];
        $selectedTracksToBeDeleted = json_decode($request->getContent())->tracks;
        try {
            foreach ($tracks as $track) {
                $creator = $track->getCourse()->getCreator();
                if (!$user->isAdmin() && $creator->getId() !== $user->getId()) {
                    return new JsonResponse(
                        [
                            "message" => "Unauthorized request",
                        ],
                        Response::HTTP_UNAUTHORIZED,
                    );
                }
                if (in_array($track->getId(), $selectedTracksToBeDeleted)) {
                    $participantMakeEventCourses = $entityManager->getRepository(ParticipantMakeEventCourse::class)->findBy(array("track" => $track));
                    if (count($participantMakeEventCourses) == 0) {
                        $entityManager->remove($track);
                    } else {
                        $events = [];
                        for ($i = 0; $i < count($participantMakeEventCourses); $i++) {
                            $events[] = $participantMakeEventCourses[$i]->getEventCourse()->getEvent()->getName();
                        }
                        $errorMessage[] = [
                            "track" => $track->getName(),
                            "events" => $events,
                        ];
                    }
                }
            }
            $entityManager->flush();
            if (empty($errorMessage)) {
                return new JsonResponse();
            } else {
                return new JsonResponse(
                    [
                        "message" => $errorMessage,
                    ],
                    Response::HTTP_CONFLICT,
                );
            }
        } catch (Exception) {
            return new JsonResponse(
                [
                    "message" => 'Something goes wrong',
                ],
                Response::HTTP_INTERNAL_SERVER_ERROR,
            );
        }
    }

    #[Route('/api/planner/my-course/delete-course/{course_id}', requirements: ['course_id' => '\d+'], methods: ['DELETE'])]
    public function deleteCourse(int $course_id, EntityManagerInterface $entityManager): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        $course = $entityManager->getRepository(Course::class)->find($course_id);
        if ($course == null) {
            return new JsonResponse(
                [
                    "message" => "Unauthorized request",
                ],
                Response::HTTP_UNAUTHORIZED
            );
        }
        $creator = $course->getCreator();
        if (!$user->isAdmin() && $creator->getId() !== $user->getId()) {
            return new JsonResponse(
                [
                    "message" => "Unauthorized request",
                ],
                Response::HTTP_UNAUTHORIZED,
            );
        }
        $participantMakeEventCourses = $entityManager->getRepository(EventCourse::class)->findBy(array("course" => $course));
        if (count($participantMakeEventCourses) > 0) {
            $eventsNames = [];
            for ($i = 0; $i < count($participantMakeEventCourses); $i++) {
                $eventsNames[] = $participantMakeEventCourses[$i]->getEvent()->getName();
            }
            return new  JsonResponse(
                $eventsNames,
                Response::HTTP_CONFLICT,
            );
        } else {
            $course->remove($entityManager);
        }
        return new  JsonResponse();
    }

    #[Route('/api/planner/my-course/missing-checkpoints/{course_id}', requirements: ['course_id' => '\d+'], methods: ['GET'])]
    public function missing_control_point(int $course_id, EntityManagerInterface $entityManager): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        $course = $entityManager->getRepository(Course::class)->find($course_id);
        if ($course == null || (!$user->isAdmin() && $course->getCreator()->getId() !== $user->getId())) {
            return new JsonResponse(
                [
                    "message" => "Unauthorized request",
                ],
                Response::HTTP_UNAUTHORIZED,
            );
        }
        $checkpoints = $course->getMissingControlPoints();
        $result = [];
        foreach ($checkpoints as $checkpoint) {
            $result[] = [
                "id" => $checkpoint->getId(),
                "checkpointId" => $checkpoint->getControlPointId(),
            ];
        }
        return new  JsonResponse(
            $result,
            Response::HTTP_OK,
        );
    }

    #[Route('/api/planner/my-course/delete-missing-checkpoint/{checkpoint_id}', requirements: ['checkpoint_id' => '\d+'], methods: ['DELETE'])]
    public function delete_missing_checkpoints(int $checkpoint_id, EntityManagerInterface $entityManager): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        $checkpoint = $entityManager->getRepository(MissingControlPoint::class)->find($checkpoint_id);
        $course = $checkpoint->getCourse();
        if (!$user->isAdmin() && $course->getCreator()->getId() !== $user->getId()) {
            return new JsonResponse(
                [
                    "message" => "Unauthorized request",
                ],
                Response::HTTP_UNAUTHORIZED,
            );
        }
        $entityManager->remove($checkpoint);
        $entityManager->flush();
        return new  JsonResponse();
    }

    #[Route('/api/planner/my-course/upload-course/{course_id}', requirements: ['course_id' => '\-?\d+'], defaults: ["course_id" => -1], methods: ['POST'])]
    public function upload_course(int $course_id, Request $request, EntityManagerInterface $entityManager): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();
        $jsonData = json_decode($request->request->get("data"), true);
        $imageData = $request->request->get("image_map");

        $isCreation = $course_id < 0;
        if ($isCreation && $imageData == null) {
            return new JsonResponse(
                [[
                    "code" => -1,
                    "message" => "Empty image"
                ]],
                Response::HTTP_BAD_REQUEST
            );
        }
        try {
            $createCourse = new CreateCourse(
                $user,
                $course_id,
                $entityManager->getRepository(Course::class)
            );
        } catch (Exception $exception) {
            return new JsonResponse(
                [[
                    "code" => -2,
                    "message" => $exception->getMessage(),
                ]],
                Response::HTTP_BAD_REQUEST
            );
        }
        try {
            $course_id = $createCourse->processData($jsonData, $imageData, $entityManager);
            $user->setModifiedDate(new DateTime("now"));
            try {
                $entityManager->persist($user);
                $entityManager->flush();
            } catch (Exception) {
                return new JsonResponse(
                    "Can't store data in database",
                    Response::HTTP_INTERNAL_SERVER_ERROR
                );
            }
            return new JsonResponse(
                ["course_id" => $course_id],
                Response::HTTP_OK
            );
        } catch (CreateCourseException $exception) {
            return new JsonResponse(
                $exception->getErrorList(),
                Response::HTTP_BAD_REQUEST
            );
        }
    }

    #[Route('/api/planner/download-stats', methods: ['GET'])]
    public function download_stats(EntityManagerInterface $entityManager): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        $courses = $entityManager->getRepository(Course::class)->findCoursesWithOrienteerCount($user);
        return self::createDownloadStats($courses);
    }

    #[Route('/api/planner/my-course/data/{course_id}', requirements: ['course_id' => '\d+'], methods: ['GET'])]
    public function course_data(int $course_id, EntityManagerInterface $entityManager): Response
    {
        $course = $entityManager->getRepository(Course::class)->find($course_id);
        if (!$course) {
            return new JsonResponse(
                [
                    "message" => "id not found",
                ],
                Response::HTTP_BAD_REQUEST
            );
        }
        /** @var User $user */
        $user = $this->getUser();
        if (!$user->isAdmin() && $course->getCreator()->getId() !== $user->getId()) {
            return new JsonResponse(
                [
                    "message" => "Unauthorized request",
                ],
                Response::HTTP_UNAUTHORIZED,
            );
        }
        return new JsonResponse(
            [
                'name' => $course->getName(),
                'club_name' => $course->getClub(),
                'club_url' => $course->getClubURL(),
                'start_date' => $course->getStartDate() == null ? null : ($course->getStartDate()->getTimeStamp() + $course->getStartDate()->getOffset()) * 1000,
                'end_date' => $course->getEndDate() == null ? null : ($course->getEndDate()->getTimeStamp() + $course->getEndDate()->getOffset()) * 1000,
                'is_printable' => $course->getPrintable(),
                'detection_radius' => $course->getDetectionRadius(),
                'discipline' => $course->getDiscipline(),
                'course_type' => $course->getCourseType(),
                'course_format' => $course->getCourseFormat(),
                'course_mode' => $course->getCourseMode(),
                'secret_key' => $course->getSecretKey(),
            ],
            Response::HTTP_OK,
        );
    }

    #[Route('/api/reset-password', methods: ['PATCH'])]
    public function reset_password(Request $request, UserPasswordHasherInterface $encoder, MailerInterface $mailer, EntityManagerInterface $entityManager): Response
    {
        $requestData = json_decode($request->getContent());
        $username = $requestData->username;
        if (strlen($username) > 30 || strlen($requestData->message) > 5000) {
            return new JsonResponse(
                "Bad request value",
                Response::HTTP_BAD_REQUEST
            );
        }
        $user = $entityManager->getRepository(User::class)->findOneBy(['username' => $username]);
        if ($user) {
            $creator = new CreateUser($user);
            $creator->forgotPassword($encoder, $entityManager);
            try {
                $message = sprintf($requestData->message, $user->getUsername(), $user->getPlainPassword());
                $creator->sendEmail($mailer, $message);
                return new JsonResponse();
            } catch (TransportExceptionInterface) {
                return new JsonResponse(
                    "Cannot send email",
                    Response::HTTP_INTERNAL_SERVER_ERROR
                );
            }
        } else {
            return new JsonResponse(
                "Unknown identifier",
                Response::HTTP_BAD_REQUEST,
            );
        }
    }

    static public function createDownloadStats($courses): JsonResponse
    {
        $results = [];
        /** @var Course $course */
        foreach ($courses as $course) {
            $results[] = [
                "id" => $course->getId(),
                "name" => $course->getName(),
                "createdAtInMilliseconds" => $course->getCreatedAt()->getTimeStamp() * 1000,
                "downloadCount" => $course->getDownloads(),
                "traceCount" => $course->getOrienteerCount(),
                "touristic" => $course->getTouristic(),
                "closed" => $course->isClosed(),
                "locked" => $course->isPrivate(),
                "discipline" => $course->getDiscipline(),
            ];
        }
        return new JsonResponse(
            $results,
            Response::HTTP_OK
        );
    }
}