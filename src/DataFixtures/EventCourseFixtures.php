<?php

namespace App\DataFixtures;

use App\Entity\EventCourse;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class EventCourseFixtures extends Fixture implements DependentFixtureInterface
{
    public const LUC_CHAMPIONSHIP_EVENT_COURSE_REFERENCE = 'Luc_championship_EventCourse';
    public const AUBIN_CHAMPIONSHIP_EVENT_COURSE_REFERENCE = 'Aubin_championship_EventCourse';
    public const LUC_POINTS_EVENT_COURSE_REFERENCE = 'Luc_points_EventCourse';
    public const AUBIN_POINTS_EVENT_COURSE_REFERENCE = 'Aubin_points_EventCourse';
    public const LUC_CUMULATED_TIME_EVENT_COURSE_REFERENCE = 'Luc_cumulated_time_EventCourse';
    public const AUBIN_CUMULATED_TIME_EVENT_COURSE_REFERENCE = 'Aubin_cumulated_time_EventCourse';

    public function load(ObjectManager $manager): void
    {
        $eventCourse = new EventCourse();
        $eventCourse->setEvent($this->getReference(EventFixtures::Championship_EVENT_REFERENCE));
        $eventCourse->setCourse($this->getReference(CourseFixtures::LUC_COURSE_REFERENCE));
        $eventCourse->setFormat(0);
        $eventCourse->setMispunchPenalty(12);
        $eventCourse->setOvertimePenalty(12);
        $eventCourse->setMaxTime(2400000);
        $manager->persist($eventCourse);
        $this->addReference(self::LUC_CHAMPIONSHIP_EVENT_COURSE_REFERENCE, $eventCourse);

        $eventCourse = new EventCourse();
        $eventCourse->setEvent($this->getReference(EventFixtures::Championship_EVENT_REFERENCE));
        $eventCourse->setCourse($this->getReference(CourseFixtures::AUBIN_COURSE_REFERENCE));
        $eventCourse->setFormat(0);
        $eventCourse->setMispunchPenalty(12);
        $eventCourse->setOvertimePenalty(12);
        $eventCourse->setMaxTime(0);
        $manager->persist($eventCourse);
        $this->addReference(self::AUBIN_CHAMPIONSHIP_EVENT_COURSE_REFERENCE, $eventCourse);


        $eventCourse = new EventCourse();
        $eventCourse->setEvent($this->getReference(EventFixtures::Points_EVENT_REFERENCE));
        $eventCourse->setCourse($this->getReference(CourseFixtures::LUC_COURSE_REFERENCE));
        $eventCourse->setFormat(0);
        $eventCourse->setMispunchPenalty(12);
        $eventCourse->setOvertimePenalty(12);
        $eventCourse->setMaxTime(0);
        $manager->persist($eventCourse);
        $this->addReference(self::LUC_POINTS_EVENT_COURSE_REFERENCE, $eventCourse);

        $eventCourse = new EventCourse();
        $eventCourse->setEvent($this->getReference(EventFixtures::Points_EVENT_REFERENCE));
        $eventCourse->setCourse($this->getReference(CourseFixtures::AUBIN_COURSE_REFERENCE));
        $eventCourse->setFormat(0);
        $eventCourse->setMispunchPenalty(12);
        $eventCourse->setOvertimePenalty(12);
        $eventCourse->setMaxTime(0);
        $manager->persist($eventCourse);
        $this->addReference(self::AUBIN_POINTS_EVENT_COURSE_REFERENCE, $eventCourse);


        $eventCourse = new EventCourse();
        $eventCourse->setEvent($this->getReference(EventFixtures::Cumulated_Time_EVENT_REFERENCE));
        $eventCourse->setCourse($this->getReference(CourseFixtures::LUC_COURSE_REFERENCE));
        $eventCourse->setFormat(0);
        $eventCourse->setMispunchPenalty(12);
        $eventCourse->setOvertimePenalty(12);
        $eventCourse->setMaxTime(0);
        $manager->persist($eventCourse);
        $this->addReference(self::LUC_CUMULATED_TIME_EVENT_COURSE_REFERENCE, $eventCourse);

        $eventCourse = new EventCourse();
        $eventCourse->setEvent($this->getReference(EventFixtures::Cumulated_Time_EVENT_REFERENCE));
        $eventCourse->setCourse($this->getReference(CourseFixtures::AUBIN_COURSE_REFERENCE));
        $eventCourse->setFormat(0);
        $eventCourse->setMispunchPenalty(12);
        $eventCourse->setOvertimePenalty(12);
        $eventCourse->setMaxTime(0);
        $manager->persist($eventCourse);
        $this->addReference(self::AUBIN_CUMULATED_TIME_EVENT_COURSE_REFERENCE, $eventCourse);

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            UserFixtures::class,
            EventFixtures::class
        ];
    }
}
