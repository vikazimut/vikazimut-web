<?php

namespace App\Model;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use PDOException;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class CreateUser
{
    private User $user;
    private string $plainPassword;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function addUser(EntityManagerInterface $entityManager, UserPasswordHasherInterface $passwordEncoder): ?array
    {
        if ($this->user->getPassword() != "") {
            $info = password_get_info($this->user->getPassword());
            if ($info['algo'] == 0) {
                if (strlen($this->user->getPassword()) < 4 || strlen($this->user->getPassword()) > 4095) {
                    return ["message" => "password invalid_length"];
                } else {
                    $encodedPassword = $passwordEncoder->hashPassword($this->user, $this->user->getPassword());
                    $this->user->setPassword($encodedPassword);
                }
            }
        } else {
            $this->plainPassword = self::generatePassword();
            $this->user->setPlainPassword($this->plainPassword);
            $encodedPassword = $passwordEncoder->hashPassword($this->user, $this->plainPassword);
            $this->user->setPassword($encodedPassword);
        }
        if (!$this->user->isValid()) {
            return ["message " => "Bad request value"];
        } else {
            try {
                $entityManager->persist($this->user);
                $entityManager->flush();
            } catch (Exception $e) {
                if (strpos($e->getMessage(), "Duplicate entry")) {
                    return ["message" => "name.already.exist"];
                } elseif (strpos($e->getMessage(), "Integrity constraint violation")) {
                    return ["message" => "error.incorrect_data"];
                } else {
                    return ["message" => $e->getMessage()];
                }
            }
            return null;
        }
    }

    public function changeUser(UserPasswordHasherInterface $passwordEncoder, EntityManagerInterface $entityManager): ?string
    {
        if (!$this->user->isValid()) {
            return "invalid user profile";
        }
        if ($this->user->getPlainPassword() != '') {
            $encodedPassword = $passwordEncoder->hashPassword($this->user, $this->user->getPlainPassword());
            $this->user->setPassword($encodedPassword);
        }
        try {
            $entityManager->persist($this->user);
            $entityManager->flush();
        } catch (PDOException $e) {
            return "Database error $e";
        }
        return null;
    }

    public function forgotPassword(UserPasswordHasherInterface $encoder, EntityManagerInterface $entityManager): void
    {
        $this->plainPassword = self::generatePassword();
        $this->user->setPlainPassword($this->plainPassword);
        $encoded = $encoder->hashPassword($this->user, $this->plainPassword);
        $this->user->setPassword($encoded);
        $entityManager->persist($this->user);
        $entityManager->flush();
    }

    /**
     * @throws TransportExceptionInterface
     */
    public function sendEmail(MailerInterface $mailer, $body): void
    {
        $message = (new Email())
            ->from(new Address('noreply@vikazimut.vikazim.fr', 'Vikazimut'))
            ->to($this->user->getEmail())
            ->subject("Vikazimut")
            ->text($body);
        $mailer->send($message);
    }

    public static function generatePassword(): string
    {
        // Random_int is supposed to be "cryptographically secure" number
        $chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

        $max = strlen($chars) - 1;
        $str = '';
        for ($i = 0; $i < 12; ++$i) {
            try {
                $str .= $chars[random_int(0, $max)];
            } catch (Exception) {
                $str .= $chars[mt_rand(0, $max)];
            }
        }

        return $str;
    }
}
