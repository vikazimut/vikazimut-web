<?php

namespace App\Model;

use App\Entity\Event;
use App\Entity\EventCourse;
use App\Entity\Participant;
use App\Repository\ParticipantMakeEventCourseRepository;
use Doctrine\ORM\EntityManagerInterface;

class ManuallySetPenaltyOfParticipantMakeEventCourse
{
    private ?Participant $participant = null;
    private ?EventCourse $eventCourse = null;
    private ?int $mispunchCount = null;
    private ?int $overtimeCount = null;

    public function setParticipant(?Participant $participant): void
    {
        $this->participant = $participant;
    }

    public function setEventCourse(?EventCourse $eventCourse): void
    {
        $this->eventCourse = $eventCourse;
    }

    public function setMispunchCount(?int $mispunchCount): void
    {
        $this->mispunchCount = $mispunchCount;
    }

    public function setOvertimeCount(?int $overtimeCount): void
    {
        $this->overtimeCount = $overtimeCount;
    }

    public function modify(Event $event, EntityManagerInterface $entityManager, ParticipantMakeEventCourseRepository $participantMakeEventCourseRepository): void
    {
        $participantMakeEventCourse = $participantMakeEventCourseRepository->find(
            [
                "eventCourse" => $this->eventCourse,
                "participant" => $this->participant
            ]);
        if ($this->overtimeCount === null) {
            $participantMakeEventCourse->setOvertimePenaltyManuallySet(false);
        } else {
            $participantMakeEventCourse->setOvertimePenaltyManuallySet(true);
            $participantMakeEventCourse->setOvertimeCount($this->overtimeCount);
        }
        if ($this->mispunchCount === null) {
            $participantMakeEventCourse->setMispunchPenaltyManuallySet(false);
        } else {
            $participantMakeEventCourse->setMispunchPenaltyManuallySet(true);
            $participantMakeEventCourse->setMispunchCount($this->mispunchCount);
        }
        $entityManager->persist($participantMakeEventCourse);
        $entityManager->flush();
        $event->updateEventScores($entityManager);
    }
}
