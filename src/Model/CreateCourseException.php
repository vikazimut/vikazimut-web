<?php

namespace App\Model;

use Exception;

class CreateCourseException extends Exception
{
    private array $_errorList;

    public function __construct($errorList)
    {
        parent::__construct();

        $this->_errorList = $errorList;
    }

    public function getErrorList(): array
    {
        return $this->_errorList;
    }
}