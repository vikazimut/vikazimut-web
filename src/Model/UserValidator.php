<?php

namespace App\Model;

class UserValidator
{
    public static function checkUsername(?string $name): bool
    {
        return $name != null && strlen($name) <= 60 && preg_match("/[ 0-9a-zA-Zá-úÁ-Ú_-]*/", $name);
    }

    public static function checkPassword(?string $password): bool
    {
        return ($password) == null || strlen($password) <= 60;
    }

    public static function checkEmail(string $email): bool
    {
        return strlen($email) <= 60 && filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    public static function checkPhone(?string $phone): bool
    {
        $phone_number_validation_regex = "/^\\+?[0-9 .-]*$/";
        return empty($phone) || (strlen($phone) <= 60 && preg_match($phone_number_validation_regex, $phone) > 0);
    }

    public static function checkValid(string $name, ?string $password, string $email, ?string $phone, ?string $lastName, ?string $firstName): bool
    {
        return self::checkUsername($name)
            && self::checkPassword($password)
            && self::checkEmail($email)
            && self::checkPhone($phone)
            && self::checkUsername($lastName)
            && self::checkUsername($firstName);
    }
}