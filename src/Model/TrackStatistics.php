<?php

namespace App\Model;

class TrackStatistics
{
    public static function distanceInMeters(float $lat1, float $lng1, float $lat2, float $lng2): float
    {
        $rad = M_PI / 180;
        $lat1r = $lat1 * $rad;
        $lat2r = $lat2 * $rad;
        $sinDLat = sin(($lat2 - $lat1) * $rad / 2);
        $sinDLon = sin(($lng2 - $lng1) * $rad / 2);
        $a = $sinDLat * $sinDLat + cos($lat1r) * cos($lat2r) * $sinDLon * $sinDLon;
        $c = 2 * asin(sqrt($a));
        return 6378137.0 * $c;
    }

    public static function computeLegLengths($simplexml): array
    {
        // 1. Create the list of checkpoints
        $root = $simplexml->RaceCourseData->Control;
        $controls = array();
        if ($root != null) {
            for ($i = 0; $i < $root->count(); $i++) {
                // Workaround for OOM bug
                $lat = $root[$i]->Position['lat'];
                $lng = $root[$i]->Position['lng'];
                // Workaround for OpenOrienteeringMapper issue: https://github.com/OpenOrienteering/mapper/issues/1947
                if ($lat == null && $lng == null) {
                    $lat = $root[$i]->CourseControl['lat'];
                    $lng = $root[$i]->CourseControl['lng'];
                }
                $controls[(string)$root[$i]->Id] = [$lat, $lng];
            }
        }
        // 2. Calculate the split distances in the order given in the xml file: <course> tags
        $root = $simplexml->RaceCourseData;
        $lengths = array();
        $previousId = null;
        if ($root->Course != null) {
            foreach ($root->Course->children() as $child) {
                if ($child->getName() === "CourseControl") {
                    $id = (string)$child->Control;
                    if ($previousId != null) {
                        $length = TrackStatistics::distanceInMeters(
                            floatval($controls[$previousId][0]),
                            floatval($controls[$previousId][1]),
                            floatval($controls[$id][0]),
                            floatval($controls[$id][1]));
                        $lengths[] = (int)$length;
                    } else {
                        $lengths[] = 0;
                    }
                    $previousId = $id;
                }
            }
        }
        return $lengths;
    }
}