<?php

namespace App\Repository;

use App\Entity\LiveTracking;
use App\Entity\LiveTrackingOrienteer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method LiveTrackingOrienteer|null find($id, $lockMode = null, $lockVersion = null)
 * @method LiveTrackingOrienteer|null findOneBy(array $criteria, array $orderBy = null)
 * @method LiveTrackingOrienteer[]    findAll()
 * @method LiveTrackingOrienteer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LiveTrackingOrienteerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LiveTrackingOrienteer::class);
    }

    public function add(LiveTrackingOrienteer $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(LiveTrackingOrienteer $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @return LiveTrackingOrienteer[] Returns an array of LiveTrackingOrienteer objects
     */
    public function findByLive($live_id): array
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.live = :id')
            ->setParameter('id', $live_id)
            ->getQuery()
            ->getResult();
    }

    public function deleteBy(LiveTracking $live): void
    {
        $this->createQueryBuilder('l')
            ->delete()
            ->andWhere('l.live = :live')
            ->setParameter('live', $live)
            ->getQuery()
            ->execute();
    }
}
