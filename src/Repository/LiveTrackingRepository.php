<?php

namespace App\Repository;

use App\Entity\LiveTracking;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method LiveTracking|null find($id, $lockMode = null, $lockVersion = null)
 * @method LiveTracking|null findOneBy(array $criteria, array $orderBy = null)
 * @method LiveTracking[]    findAll()
 * @method LiveTracking[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LiveTrackingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LiveTracking::class);
    }

    public function add(LiveTracking $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(LiveTracking $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);
        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @return LiveTracking[] Returns an array of LiveTracking objects in decreasing order of id
     */
    public function findByCourse($course_id): array
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.course = :id')
            ->setParameter('id', $course_id)
            ->orderBy('l.id', 'DESC')
            ->getQuery()
            ->getResult();
    }
}
