Script for cleaning the Vikazimut folder

1. clean_images.sh to remove unused maps

2. clean_token.sh to delete the invalid refresh token from the database

3. clean_tracks.sh to remove tracks older than 5 months
