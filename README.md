# Le serveur web Vikazimut

## Le projet Vikazimut

Vikazimut est un projet d'étudiants en informatique de l'[ENSICAEN](https://www.ensicaen.fr).

Le projet répond à une demande du club normand de course d'orientation et de raids multisports [Vik'Azim](https://vikazim.fr)
visant la réalisation d'une application mobile pour la pratique de la course d'orientation
en autonomie.

L’application mobile Vikazimut remplace la carte papier, la boussole et le poinçon de validation des postes de contrôle.
Un parcours d'orientation consiste en une suite de postes de contrôle matérialisés sur le terrain
par une balise type fédération internationale de course d’orientation (IOF).
L’orienteur utilise l’application pour se repérer à partir de la carte et
valider son passage aux points de contrôle en utilisant le lecteur de code QR,
le lecteur de tag NFC ou le lecteur de iBeacons selon l'équipement qui est associé au poste de contrôle.
Un mode automatique permet la validation directement à partir de la
position GPS sans utiliser d'équipement physique au poste de contrôle.

Le site web compagnon fournit les parcours d'orientation sous la forme
d'une carte avec les positions des postes de contrôle.
Ces parcours sont proposés par des clubs, des enseignants d'EPS, des collectivités ou des
particuliers qui ont un compte sur le site.
Le site web offre aussi des outils d'analyse rétrospective de la performance réalisée
pour les parcours dont les orienteurs ont volontairement téléversé la trace sur le site.

## Le serveur web

La présente forge correspond au serveur web (backend) du site web [https://vikazimut.vikazim.fr](https://vikazimut.vikazim.fr)
et de l'application mobile. Il est développé en Symfony/Php.

Le serveur gère les parcours d'orientation créés par des traceurs authentifiés sur le serveur.
Le serveur fournit les données à l'application mobile et au site web.
À l'application mobile, le serveur envoie les parcours disponibles et recupère
les traces des parcours réalisés.
Au site web, le serveur fournit et reçoit les données des parcours, des événements et des traces.

## Code source

Le projet est développé avec le kit de développement Symfony et le langage Php.
Le code source du serveur web Vikazimut est accessible sur le GitLab du projet&nbsp;:
[https://gitlab.com/clouardregis/vikazimut-server](https://gitlab.com/clouardregis/vikazimut-server).

## Licence

Le projet est à code source ouvert, libre et gratuit.
Il est placé sous la licence publique générale GNU
amoindrie [GNU Lesser General Public License](https://www.gnu.org/licenses/lgpl-3.0.html).

    Licence LGPL V3

    Ce programme est un logiciel libre ; vous pouvez le redistribuer et/ou le modifier conformément
    aux dispositions de la Licence Publique Générale GNU, telle que publiée par la Free Software
    Foundation ; version 2 de la licence, ou encore (à votre choix) toute version ultérieure.

    Ce programme est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE ; sans même
    la garantie implicite de COMMERCIALISATION ou D'ADAPTATION A UN USAGE PARTICULIER.
    Pour plus de détail, voir la Licence Publique Générale GNU.

## Projets liés

- L'application mobile développée en Flutter - Android et iOS ([dépôt gitlab](https://gitlab.ecole.ensicaen.fr/rclouard/vikazimut-app)).
- Le site web développé en Flutter - partie frontend ([dépôt gitlab](https://gitlab.com/clouardregis/vikazimut-website)).

## Contributeurs ([Université de Caen Normandie](https://www.unicaen.fr) - [spécialité informatique](https://www.info.unicaen.fr/licence/info))

### 2019/2020

- Martin FÉAUX DE LA CROIX

### 2020/2021

- Antoine BOITEAU
- Suliac LAVENANT
