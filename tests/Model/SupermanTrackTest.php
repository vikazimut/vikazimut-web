<?php

namespace App\Tests\Model;

use App\Model\SupermanTrack;
use PHPUnit\Framework\TestCase;

class SupermanTrackTest extends TestCase
{
    public function testIsCompleteTrackEmptyPunches()
    {
        $punchTimes = [];
        $actual = SupermanTrack::isCompleteAndNotForcedTrack($punchTimes);
        $this->assertFalse($actual);
    }

    public function testIsCompleteTrackNominal()
    {
        $punchTimes = [
            ['controlPoint' => 0, 'punchTime' => 0],
            ['controlPoint' => 1, 'punchTime' => 10],
            ['controlPoint' => 2, 'punchTime' => 20],
            ['controlPoint' => 3, 'punchTime' => 30],
        ];
        $actual = SupermanTrack::isCompleteAndNotForcedTrack($punchTimes);
        $this->assertTrue($actual);
    }

    public function testIsCompleteTrackWhenMissingPunch()
    {
        $punchTimes = [
            ['controlPoint' => 0, 'punchTime' => 0],
            ['controlPoint' => 1, 'punchTime' => 10],
            ['controlPoint' => 2, 'punchTime' => -1],
            ['controlPoint' => 3, 'punchTime' => 20],
            ['controlPoint' => 4, 'punchTime' => 30],
        ];
        $actual = SupermanTrack::isCompleteAndNotForcedTrack($punchTimes);
        $this->assertFalse($actual);
    }

    public function testIsCompleteTrackWhenBackCompatibility()
    {
        $punchTimes = [
            ['controlPoint' => 0, 'punchTime' => 0],
            ['controlPoint' => 1, 'punchTime' => 10],
            ['controlPoint' => 2, 'punchTime' => 0],
            ['controlPoint' => 3, 'punchTime' => 20],
            ['controlPoint' => 4, 'punchTime' => 30],
        ];
        $actual = SupermanTrack::isCompleteAndNotForcedTrack($punchTimes);
        $this->assertFalse($actual);
    }

    public function testBuildLegRouteWithBestLegWhenFirstLeg()
    {
        $bestTrack = new SupermanTrack();
        /* Route: list of waypoints: [latitude, longitude, timeInMs, elevation] */
        $route = [
            [0, 0, 0, 0],
            [1, -1, 1, 0],
            [2, -2, 2, 0],
            [3, -3, 10, 0],
            [4, -4, 11, 0],
        ];
        $minTime = 0;
        $maxTime = 10;
        $shift = 0;
        $bestTrack->buildLegRouteWithBestLeg($route, $minTime, $maxTime, $shift);
        $this->assertCount(4, $bestTrack->getRoute());
        for ($i = 0; $i < count($bestTrack->getRoute()); $i++) {
            $waypoint = $bestTrack->getRoute()[$i];
            $this->assertEquals($route[$i][0], $waypoint[0]);
            $this->assertEquals($route[$i][1], $waypoint[1]);
            $this->assertEquals($route[$i][2] - $shift, $waypoint[2]);
            $this->assertEquals($route[$i][3], $waypoint[3]);
        }
    }

    public function testBuildLegRouteWithBestLegWhenAllLegs()
    {
        $bestTrack = new SupermanTrack();
        /* Route: list of waypoints: [latitude, longitude, timeInMs, elevation] */
        $route = [
            [0, 0, 0, 0],
            [1, -1, 1, 0],
            [2, -2, 2, 0],
            [3, -3, 10, 0],
            [4, -4, 11, 0],
            [5, -5, 12, 0],
            [6, -6, 13, 0],
            [7, -7, 14, 0],
        ];
        $minTime = 0;
        $maxTime = 10;
        $shift = 0;
        $bestTrack->buildLegRouteWithBestLeg($route, $minTime, $maxTime, $shift);
        $this->assertCount(4, $bestTrack->getRoute());
        for ($i = 0; $i < count($bestTrack->getRoute()); $i++) {
            $waypoint = $bestTrack->getRoute()[$i];
            $this->assertEquals($route[$i][0], $waypoint[0]);
            $this->assertEquals($route[$i][1], $waypoint[1]);
            $this->assertEquals($route[$i][2] - $shift, $waypoint[2]);
            $this->assertEquals($route[$i][3], $waypoint[3]);
        }
        $minTime = 10;
        $maxTime = 14;
        $bestTrack->buildLegRouteWithBestLeg($route, $minTime, $maxTime, $shift);
        $this->assertCount(9, $bestTrack->getRoute());
        for ($i = 4; $i < count($bestTrack->getRoute()); $i++) {
            $waypoint = $bestTrack->getRoute()[$i];
            $this->assertEquals($route[$i - 1][0], $waypoint[0]);
            $this->assertEquals($route[$i - 1][1], $waypoint[1]);
            $this->assertEquals($route[$i - 1][2] - $shift, $waypoint[2]);
            $this->assertEquals($route[$i - 1][3], $waypoint[3]);
        }
    }

    public function testBuildLegRouteWithBestLeg2Tracks()
    {
        $bestTrack = new SupermanTrack();
        /* Route: list of waypoints: [latitude, longitude, timeInMs, elevation] */
        $route1 = [
            [0, 0, 0, 0],
            [1, -1, 1, 0],
            [2, -2, 2, 0],
            [3, -3, 10, 0],
            [4, -4, 11, 0],
            [5, -5, 12, 0],
            [6, -6, 13, 0],
            [7, -7, 34, 0],
        ];
        $minTime = 0;
        $maxTime = 10;
        $shift = 0;
        $bestTrack->buildLegRouteWithBestLeg($route1, $minTime, $maxTime, $shift);
        $this->assertCount(4, $bestTrack->getRoute());
        for ($i = 0; $i < count($bestTrack->getRoute()); $i++) {
            $waypoint = $bestTrack->getRoute()[$i];
            $this->assertEquals($route1[$i][0], $waypoint[0]);
            $this->assertEquals($route1[$i][1], $waypoint[1]);
            $this->assertEquals($route1[$i][2] - $shift, $waypoint[2]);
            $this->assertEquals($route1[$i][3], $waypoint[3]);
        }

        $route2 = [
            [0, 0, 0, 0],
            [1, -1, 1, 0],
            [2, -2, 2, 0],
            [3, -3, 20, 0],
            [4, -4, 21, 0],
            [5, -5, 22, 0],
            [6, -6, 23, 0],
            [7, -7, 24, 0],
        ];
        $minTime = 20;
        $maxTime = 24;
        $shift = 10;
        $bestTrack->buildLegRouteWithBestLeg($route2, $minTime, $maxTime, $shift);
        $this->assertCount(9, $bestTrack->getRoute());
        for ($i = 4; $i < count($bestTrack->getRoute()); $i++) {
            $waypoint = $bestTrack->getRoute()[$i];
            $this->assertEquals($route2[$i - 1][0], $waypoint[0]);
            $this->assertEquals($route2[$i - 1][1], $waypoint[1]);
            $this->assertEquals($route2[$i -1][2] - $shift, $waypoint[2]);
            $this->assertEquals($route2[$i - 1][3], $waypoint[3]);
        }
    }
}
